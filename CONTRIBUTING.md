# Contributing

Would you like to contribute? There are 2 ways:

1. Fork this project, commit your changes there, and do a Merge Request against
   this project. Don't forget to assign either @falconierif or @hoverht as
   reviewer to have a look at your code.
2. Ask us to become a Developer in this repo, so you can develop in a branch
   here.

## Contribution Flow

1. Contributor opens a merge request with a
   [conventional commit title](https://www.conventionalcommits.org/en/v1.0.0/#summary)
   and a clear summary of the change proposed. By default the title should start
   with `Draft: `.
2. When the merge request is ready (Pipelines are all finished and green), the
   contributor should `Mark as ready` (= title no longer starts with `Draft: `),
   and assign a reviewer.
3. Reviewer should directly implement simple feedback in a separate merge
   request to be merged into the merge request under review.
   - When feedback is much more complex, or discussion is needed, reviewer
     should not implement suggested changes to avoid unnecessary work and
     unnecessary cognitive load on all parties. First discuss the feedback in
     the comments, then discuss the what/how/who of implementing changes.
4. The philosophy should be to move fast and break things, that's why we pin
   versions.

### pre-commit hooks

We use pre-commit hooks in this project. You don't have to use them, but if you
want you can install them this way:

```bash
pip install pre-commit
pre-commit install
pre-commit install --hook-type commit-msg
pre-commit install --hook-type post-commit
```

### testing

We try to keep the code coverage as high as possible here, so if you contribute
with a functionality please add a test for it. How do we test things? We use
Gitlab-CI to test our gitlab ci templates of jobs and pipelines! This way we
know we don't break stuff in your downstream project. `tests/jobs` is where we
keep jobs tests, `tests/pipelines` is where we keep pipeline tests.
`tests/mockup_projects` is where we keep fake projects to test our jobs and
pipelines on. We use
[child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html)
to keep things neat and separate logically separated jobs (different
language/pipeline scenario). For example if you contribute a python job, please
add it to the `include` block in `tests/jobs/python.yml`.

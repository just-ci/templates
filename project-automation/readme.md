# Project Automation

## External Repository Update Check

Include the following job in a template which you include elsewhere. This allows
to automatically check if the included template has been updated externally.

```yaml
include:
  - remote: https://jobs.just-ci.dev/v6.31.2/project-automation/update-check.yml

update-check:your-repo-name:
  extends: .update-check
  variables:
    UPDATE_CHECK_CURRENT_VERSION: v1.2.3
    UPDATE_CHECK_REPOSITORY: https://optional:authentication@gitlab.example.com/your/template/repo.git
    UPDATE_CHECK_NAME: Your optional repository name
```

> Make sure the variable `UPDATE_CHECK_CURRENT_VERSION` is updated whenever your
> template is updated

## Pipeline Scheduler

Add multiple variables using a multiline variable.

```yaml
variables:
  SCHEDULE_PIPELINE_VARIABLES: |
    VARIABLE_KEY=VARIABLE_VALUE
    VARIABLE_KEY2=VARIABLE_VALUE2
```

## Version pin check

To ensure versions are pinned using variables, you can extend the
`.version-pinning` job to check if a variable is set. For example:

```yaml
include:
  - remote: https://jobs.just-ci.dev/v6.31.2/project-automation/version-pin-check.yml

PIN-VERSION:PYVERSION:
  extends: .version-pin-check
  rules:
    - if: '$I_DONT_PIN_MY_VERSIONS == null && $PYVERSION !~ /^3\.[0-9]{1,2}.*$/'
```

> Ensure the job name has the variable name after the first `:` for the message
> to be correct.
